import os

TG_TOKEN = os.environ.get("TG_BOT_TOKEN")
PROXY = None
CHAT_ID = int(
    os.environ.get("CHAT_ID", "-581025811")
)  # The chat group or individual chat id of forwarded message destination
BOT_TIMEOUT = 10

DB_PATH = os.environ.get("DB_PATH")
