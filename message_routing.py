from db import connection
from telegram import Message
from config import CHAT_ID


def find_chat_id_to_forward(message: Message):
    cursor = connection.execute(
        "SELECT forward_to_chat_id FROM message_routing WHERE id=?", (message.chat_id,)
    )
    rows = cursor.fetchall()
    if len(rows):
        res = rows[0]
        return res[0]
    return get_default_chat_id_to_forward()


def get_default_chat_id_to_forward():
    return CHAT_ID


def is_chat_id_where_messages_are_forwarded(chat_id: str):
    if chat_id == CHAT_ID:
        return True
    res = connection.fetch_one(
        "SELECT COUNT(*) FROM message_routing WHERE forward_to_chat_id=?", (chat_id,)
    )
    return res[0] > 0
