from enum import Enum
from typing import Optional, Callable, Dict

from telegram import Message, Update
from telegram.ext import CallbackContext

from telegram import Message

from features.banned_chat import is_ban_command, ban_chat, unban_chat, is_unban_command
from features.info_about_author import is_info_command, send_info_about_user


class AdminCommand(Enum):
    ban = "ban"
    unban = "unban"
    info = "info"


CommandHandler = Callable[[Update, CallbackContext], None]

COMMAND_TO_HANDLER: Dict[AdminCommand, CommandHandler] = {
    AdminCommand.ban: ban_chat,
    AdminCommand.unban: unban_chat,
    AdminCommand.info: send_info_about_user,
}


def is_admin_command(command: str) -> bool:
    if not command:
        return False
    admin_command = get_admin_command(command)
    return bool(admin_command)


def handle_admin_command(update: Update, context: CallbackContext):
    message: Message = update.message
    command = message.text
    admin_command = get_admin_command(command)
    if not admin_command:
        raise Exception(f"{command} is not an admin command")
    handler = COMMAND_TO_HANDLER.get(admin_command)
    if not handler:
        raise Exception(f'No handler for {admin_command}')
    handler(update, context)


def get_admin_command(command: str) -> Optional[AdminCommand]:
    if is_ban_command(command):
        return AdminCommand.ban
    if is_unban_command(command):
        return AdminCommand.unban
    if is_info_command(command):
        return AdminCommand.info
    return None
