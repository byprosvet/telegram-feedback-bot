from loguru import logger
from telegram import Message, Update
from telegram.ext import CallbackContext

import feedback


def send_info_about_user(update: Update, context: CallbackContext):
    message: Message = update.message
    feedback_message = feedback.find_feedback(update.message.reply_to_message.message_id)
    logger.info("[Get user info] {}", feedback_message.user_fullname)
    context.bot.send_message(
        chat_id=message.chat_id,
        text=f"""
User ID = <a href="tg://user?id={feedback_message.user_id}">{feedback_message.user_id}</a>
Fullname = {feedback_message.user_fullname or ''}
Username = {'@' + feedback_message.username if feedback_message.username else 'unknown'}
        """,
        reply_to_message_id=message.message_id,
        parse_mode='HTML'
    )


def is_info_command(command: str) -> bool:
    if command == "/info" or command == "/инфо":
        return True
    return False
