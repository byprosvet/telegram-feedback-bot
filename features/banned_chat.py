import datetime

from loguru import logger
from telegram import Update
from telegram.ext import CallbackContext

import feedback
from db import connection


def is_chat_banned(chat_id: int):
    cursor = connection.execute(
        "SELECT chat_id FROM banned_chats WHERE chat_id=?", (chat_id,)
    )
    rows = cursor.fetchall()
    return bool(len(rows))


def ban_chat(update: Update, context: CallbackContext):
    feedback_message = feedback.find_feedback(update.message.reply_to_message.message_id)
    received_message_chat_id = feedback_message.received_message_chat_id
    logger.info("[Ban chat] {}", received_message_chat_id)
    connection.execute(
        "INSERT OR IGNORE INTO banned_chats VALUES (?, ?)",
        (
            received_message_chat_id,
            datetime.datetime.now(),
        ),
    )


def unban_chat(update: Update, context: CallbackContext):
    feedback_message = feedback.find_feedback(update.message.reply_to_message.message_id)
    received_message_chat_id = feedback_message.received_message_chat_id
    logger.info("[Unban chat] {}", received_message_chat_id)
    connection.execute(
        "DELETE FROM banned_chats WHERE chat_id=?", (received_message_chat_id,)
    )


def is_ban_command(command: str) -> bool:
    if command == "/ban" or command == "/бан":
        return True
    return False


def is_unban_command(command: str) -> bool:
    if command == "/unban" or command == "/отбан":
        return True
    return False
