from typing import List, Optional, Union

from telegram import Message, TelegramObject, Update
from telegram.ext import CallbackContext
from telegram.error import TelegramError

from loguru import logger
import json

from config import BOT_TIMEOUT

import bot_automessages
import feedback
from features.admin_command import is_admin_command, handle_admin_command
import message_routing


def prepare_params(
    attachment: Union[TelegramObject, List[TelegramObject]], at: str
) -> Optional[dict]:
    """prepare params for bots' ``send_*``."""
    if isinstance(attachment, list):
        if len(attachment) > 0:
            attachment = attachment[0]
        else:
            return None

    params = {}
    unprepared_params = attachment.to_dict()

    logger.debug(json.dumps(unprepared_params))

    file_id = unprepared_params.pop("file_id", None)
    if file_id is not None:
        params[at] = file_id

    return params


def reply_to_bot_message(update: Update, context: CallbackContext):
    message: Message = update.message
    if message_routing.is_chat_id_where_messages_are_forwarded(message.chat.id):
        logger.info("[Reply to message] {}", message)
        try:
            feedback_message: feedback.FeedbackMessage = feedback.find_feedback(message.reply_to_message.message_id)
        except feedback.EntityNotFound:
            logger.warning("[Not found forwarded message] {}", message.reply_to_message.message_id)
            return
        logger.info("[Found forwarded message] {}", feedback_message)
        try:
            if is_admin_command(message.text):
                logger.debug("[Admin command] {}", message.text)
                handle_admin_command(update, context)
                return
            send_message(
                context.bot,
                feedback_message.received_message_chat_id,
                message,
                feedback_message.received_message_id,
            )
        except TelegramError as e:
            logger.info("[Unable to send message] {}", e)
            message_text = bot_automessages.error_replying_message() + "\n" + e.message
            context.bot.send_message(
                chat_id=message.chat_id,
                text=message_text,
                reply_to_message_id=message.message_id,
            )


def send_message(bot, chat_id, message, reply_to_message_id=None):
    params = {"chat_id": chat_id, "timeout": BOT_TIMEOUT}
    if reply_to_message_id is not None:
        params["reply_to_message_id"] = reply_to_message_id

    logger.info("[Sending message as bot] {}", params)
    try:
        if message.text is not None:
            params["text"] = message.text
            bot.send_message(**params)
        if message.effective_attachment is not None:
            for at in message.ATTACHMENT_TYPES:
                attachment: Union[TelegramObject, List[TelegramObject]] = getattr(
                    message, at
                )
                if attachment is not None:
                    attachment_params = prepare_params(attachment, at)
                    if attachment_params is None:
                        continue
                    params.update(attachment_params)
                    if message.caption is not None:
                        params["caption"] = message.caption
                    bot_method = getattr(bot, f"send_{at}")
                    bot_method(**params)

                    break
    except TelegramError as e:
        if reply_to_message_id is None:
            raise e
        send_message(bot, chat_id, message)
