from db import connection
import sys, getopt

def main(argv):
  query = ''
  try:
    opts, args = getopt.getopt(argv,"x",["query="])
  except getopt.GetoptError:
    print('run.py --query=<query>')
    sys.exit(2)
  for opt, arg in opts:
    if opt == '-h':
      print('run.py --query=<query>')
      sys.exit()
    elif opt in ("--query"):
      query = arg
  cursor = connection.execute(query)
  print("Result", cursor.fetchall())

if __name__ == "__main__":
   main(sys.argv[1:])
