from db import connection
import feedback
from unittest.mock import Mock

def before():
    connection.execute("DELETE FROM forwarded_messages")

def test_forwarding_message():
    before()
    chat_attrs = {"id": "chat-123"}
    received_attrs = {"chat": Mock(**chat_attrs), "message_id": "received-1"}
    received_message = Mock(**received_attrs)
    forwarded_attrs = {"message_id": "sent-1"}
    forwarded_message = Mock(**forwarded_attrs)
    feedback.add_forwarded_message(received_message, forwarded_message)
    feedback_message = feedback.find_feedback("sent-1")
    assert feedback_message.received_message_id == "received-1"
    assert feedback_message.received_message_chat_id == "chat-123"

def test_replying_to_non_existing_message():
    before()
    try:
        feedback.find_feedback("sent-1")
        assert False
    except feedback.EntityNotFound:
        assert True
