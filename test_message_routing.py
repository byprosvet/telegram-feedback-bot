import message_routing
from db import connection
from unittest.mock import Mock
from config import CHAT_ID


def before():
    connection.execute("DELETE FROM message_routing")


def test_finding_default_chat():
    before()
    message = Mock(chat_id="-20")
    chat_id = message_routing.find_chat_id_to_forward(message)
    assert chat_id == CHAT_ID


def test_finding_non_default_chat():
    before()
    connection.execute("INSERT INTO message_routing VALUES ('-20', '-2')")
    message = Mock(chat_id="-20")
    chat_id = message_routing.find_chat_id_to_forward(message)
    assert chat_id == "-2"


def test_default_chat_is_chat_where_messages_are_forwarded():
    before()
    assert message_routing.is_chat_id_where_messages_are_forwarded(CHAT_ID)


def test_non_default_chat_is_not_chat_where_messages_are_forwarded():
    before()
    assert message_routing.is_chat_id_where_messages_are_forwarded("-2") == False


def test_non_default_chat_is_chat_where_messages_are_forwarded():
    before()
    connection.execute("INSERT INTO message_routing VALUES ('-20', '-2')")
    assert message_routing.is_chat_id_where_messages_are_forwarded("-2")
