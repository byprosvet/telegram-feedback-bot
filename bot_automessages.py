from db import connection

def welcome_message():
    res = connection.fetch_one("SELECT message FROM bot_message_texts WHERE id='welcome'")
    return res[0]

def help_message():
    res = connection.fetch_one("SELECT message FROM bot_message_texts WHERE id='help'")
    return res[0]

def error_replying_message():
    res = connection.fetch_one("SELECT message FROM bot_message_texts WHERE id='error_replying'")
    return res[0]
