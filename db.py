import sqlite3
from typing import List, Any, Tuple, Union
from config import DB_PATH

class DBConnection:
    def __init__(self, DB_PATH: str = None):
        if (DB_PATH is None):
            raise ValueError("DB_PATH is not provided")

        self.DB_PATH = DB_PATH

    def connect(self):
        return sqlite3.connect(self.DB_PATH)

    def execute(self, sql: str, params: Union[List[Any], Tuple] = []):
        with self.connect() as conn:
            cursor = conn.cursor()
            return cursor.execute(sql, params)


    def fetch_one(self, sql: str, params: Union[List[Any], Tuple] = []):
        cursor = self.execute(sql, params)
        return cursor.fetchone()

connection = DBConnection(DB_PATH)
