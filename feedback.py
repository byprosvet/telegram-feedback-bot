import datetime
from dataclasses import dataclass
from typing import Optional

from loguru import logger
from telegram import Message

from db import connection


class EntityNotFound(Exception):
    pass


@dataclass
class FeedbackMessage:
    received_message_chat_id: int
    received_message_id: int
    forwarded_message_id: int
    username: Optional[str]
    user_fullname: Optional[str]
    user_id: int


def add_chat(chat_id: str):
    connection.execute(
        "INSERT OR IGNORE INTO all_chats VALUES (?, ?)",
        (
            chat_id,
            datetime.datetime.now(),
        ),
    )


def add_forwarded_message(received_message: Message, forwarded_message: Message):
    logger.info("Forwarded message = {}", forwarded_message)
    connection.execute(
        "INSERT INTO forwarded_messages VALUES (?, ?, ?, ?, ?, ?, ?)",
        (
            received_message.message_id,
            received_message.chat.id,
            forwarded_message.message_id,
            datetime.datetime.now(),
            received_message.from_user.username,
            received_message.from_user.id,
            received_message.from_user.full_name,
        ),
    )


def find_feedback(forwarded_message_id) -> FeedbackMessage:
    cursor = connection.execute(
        "SELECT * FROM forwarded_messages WHERE forwarded_message_id = ?",
        (str(forwarded_message_id),),
    )
    rows = cursor.fetchall()
    if (len(rows) < 1):
        raise EntityNotFound(f'forwarded_message_id not found: {forwarded_message_id}')
    res = rows[0]
    return FeedbackMessage(
        received_message_id=res[0],
        received_message_chat_id=res[1],
        forwarded_message_id=res[2],
        username=res[4],
        user_id=res[5],
        user_fullname=res[6],
    )
