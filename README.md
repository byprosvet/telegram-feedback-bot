# Telegram Feedback bot
This work is based on [Python bot by lichengqi0805](https://github.com/lichengqi0805/Telegram-feedback-bot), thanks to their great work.

## Environment requirement
Python 3

## Get started
1. Get Telegram bot api id from @Botfather

2. Set the following env vars: `TG_BOT_TOKEN`, `CHAT_ID`, `WELCOME_MESSAGE`, `HELP_MESSAGE`

3. Run with ``` python main.py ``` or docker image
