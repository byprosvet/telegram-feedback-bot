from db import connection


def migrate(sql: str, params=[]):
    connection.execute(sql, params)
    print("Migrated", sql)


migrate(
    """CREATE TABLE IF NOT EXISTS all_chats (
    chat_id VARCHAR(255) NOT NULL,
    collected_at timestamp with time zone,
    PRIMARY KEY (chat_id)
)"""
)

migrate(
    """CREATE TABLE IF NOT EXISTS forwarded_messages (
    received_message_id VARCHAR(255) NOT NULL,
    received_message_chat_id VARCHAR(255),
    forwarded_message_id VARCHAR(255),
    collected_at timestamp with time zone,
    PRIMARY KEY (received_message_id)
)"""
)

migrate(
    """CREATE TABLE IF NOT EXISTS message_routing (
    id VARCHAR(255) NOT NULL,
    forward_to_chat_id VARCHAR(255),
    PRIMARY KEY (id)
)"""
)

migrate(
    """CREATE TABLE IF NOT EXISTS bot_message_texts (
    id VARCHAR(255) NOT NULL,
    message VARCHAR(255),
    PRIMARY KEY (id)
)"""
)

migrate(
    """
    INSERT OR IGNORE INTO bot_message_texts VALUES
    ('welcome', "Welcome"),
    ('help', "Help"),
    ('error_replying', "Unable to send message")
"""
)

migrate(
    """CREATE TABLE IF NOT EXISTS banned_chats (
    chat_id VARCHAR(255) NOT NULL,
    collected_at timestamp with time zone,
    PRIMARY KEY (chat_id)
)"""
)

try:

    migrate(
        """ALTER TABLE forwarded_messages
      ADD COLUMN username VARCHAR(255)
    """
    )

    migrate(
        """ALTER TABLE forwarded_messages
      ADD COLUMN user_id VARCHAR(255)
    """
    )

    migrate(
        """ALTER TABLE forwarded_messages
      ADD COLUMN user_fullname VARCHAR(255)
    """
    )
except:
    pass
