from loguru import logger
from telegram import Message, Update, ChatAction
from telegram.ext import CallbackContext

import bot_automessages
import feedback
import message_routing
from config import BOT_TIMEOUT
from send_action import send_action
from features.banned_chat import is_chat_banned
from features.reply_to_bot import reply_to_bot_message


@send_action(ChatAction.TYPING)
def start_callback(update: Update, context: CallbackContext):
    """Send a message when the command ``/start`` is issued."""
    message = update.message
    logger.info("[Start command received] {}", message)
    feedback.add_chat(message.chat.id)
    message.reply_text(bot_automessages.welcome_message())


@send_action(ChatAction.TYPING)
def help_callback(update: Update, context: CallbackContext):
    """Send a message when the command ``/help`` is issued."""
    message = update.message
    logger.info("[Help command received] {}", message)
    feedback.add_chat(message.chat.id)
    message.reply_text(bot_automessages.help_message())


def reply_callback(update: Update, context: CallbackContext):
    message: Message = update.message
    if message_routing.is_chat_id_where_messages_are_forwarded(message.chat.id):
        reply_to_bot_message(update, context)
    else:
        forward_callback(update, context)


def forward_callback(update: Update, context: CallbackContext):
    """Forward the user message to ``CHAT_ID``."""
    logger.info("[Received message] {}", update)
    message: Message = update.message
    if message_routing.is_chat_id_where_messages_are_forwarded(message.chat.id):
        pass
    else:
        if is_chat_banned(message.chat.id):
            logger.info(
                "[Chat was banned] {} {}", message.chat.id, message.chat.username
            )
            return
        feedback.add_chat(message.chat.id)
        chat_id = message_routing.find_chat_id_to_forward(message)
        logger.info("[Found chat id to forward] {} {}", message.chat_id, chat_id)
        forwarded_message = context.bot.forward_message(
            chat_id=chat_id,
            from_chat_id=message.chat_id,
            message_id=message.message_id,
            timeout=BOT_TIMEOUT,
        )
        feedback.add_forwarded_message(message, forwarded_message)


def error(update: Update, context: CallbackContext):
    """Log Errors caused by Updates."""
    params = {error: context.error, update: update}
    logger.warning("[TG Error] {}", params)
